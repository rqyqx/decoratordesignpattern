public class AndroidPhones extends PhoneDecorator {
    public AndroidPhones(Phones p) {
        super(p);
    }

    @Override
    public void function() {
        super.function();
        System.out.print(" Adding features of Android phones.");
    }
}
