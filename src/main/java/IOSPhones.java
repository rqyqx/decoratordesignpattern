public class IOSPhones extends PhoneDecorator {
    public IOSPhones(Phones p) {
        super(p);
    }

    @Override
    public void function() {
        super.function();
        System.out.print(" Adding features of IOS phones.");
    }
}
