public class PhoneDecorator implements Phones {
    protected Phones phone;

    public PhoneDecorator(Phones p) {
        this.phone = p;
    }

    @Override
    public void function() {
        this.phone.function();
    }
}

