public class DecorateTester {
    public static void main(String[] args) {
        Phones iosPhone = new IOSPhones(new CellPhones());
        iosPhone.function();
        System.out.println("\n");

        Phones androidPhone = new AndroidPhones(new CellPhones());
        androidPhone.function();
        System.out.println("\n");

        Phones IOSAndroidPhone = new AndroidPhones(new IOSPhones(new CellPhones()));
        IOSAndroidPhone.function();
        System.out.println("\n");
    }
}
